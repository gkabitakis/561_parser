'use strict'
var fs = require("fs");
var fileName;
/**
 * Helper Variables
 */
var URI = '<http://www.csd.uoc.gr/ontology/2019/';
var dbName = URI;
var newline = ' .\n';
var quotes = '"';
var space = ' ';
var type = '';
var lang = '@en';

if (process.argv[2]) {
	fileName = process.argv[2];
	if (process.argv[2] === '-h') {
		console.log('Usage >node script.js jsonFile outputFile');
		return;
	}
	try {
		var json = require('./jsons/' + fileName);
		var data;
		switch (fileName) {
			case 'classrooms.json':
				data = parseClassrooms(json);
				break;
			case 'courses.json':
				data = parseCourses(json);
				break;
			case 'people.json':
				data = parsePeople(json);
				break;
			case 'programmingLanguage.json':
				data = parseProgrammingLanguage(json);
				break;
			case 'subjects.json':
				data = parseSubjects(json);
				break;
			default:
				console.error('No parser specified for this file')
				return;
		}
		// console.log(data); //For debugging

		write2File(process.argv[3] ? process.argv[3] : 'unknown.txt', data, (err) => {
			if (err) console.error(err);
			else {
				console.log('File was succesfully created');
			}
		});
	} catch (e) {
		console.log(e);
	}
} else {
	console.log('Usage >node script.js jsonFile outputFile');
	return;
}


function write2File(fileName, data, callback) {
	fs.writeFile(fileName, data, (err) => {
		callback(err);
	});
}

function parseClassrooms(json) {
	var createdString = '';
	for (let i = 0; i < json.length; i++) {
		var localURI = URI + json[i].name.replace(/\s/g, '') + '>';
		if (json[i].name === 'Amphitheatro A' || json[i].name === 'Amphitheatro B') {
			type = 'Amphitheater';
		} else type = 'Aithousa';

		createdString += localURI + ' rdf:type ' + URI + type + '>' + newline;
		createdString += localURI + space + dbName + 'capacity>' + space + quotes + json[i].capacity + quotes + newline; //Type
	}
	return createdString;
}

function parseCourses(json) {
	var createdString = '';
	//TODO: Question about the type is it only PostGraduateCourse UnderGraduateCourse
	for (let i = 0; i < json.length; i++) {
		var localURI = URI + json[i].course_code_en + '>';
		switch (json[i].program_name) {
			case 'Προπτυχιακό πρόγραμμα':
				type = 'UnderGraduateCourse';
				break;
			case 'Μεταπτυχιακό πρόγραμμα':
				type = 'PostGraduateCourse';
				break;
		}
		createdString += localURI + ' rdf:type ' + URI + type + '>' + newline; //Type
		createdString += localURI + space + dbName + 'course_code>' + space + quotes + json[i].course_code_en + quotes + lang + newline; //Code
		createdString += localURI + space + dbName + 'course_name>' + space + quotes + json[i].course_name_en + quotes + lang + newline; //Name
		//TODO what about the newlines and tabs that exist in string
		createdString += localURI + space + dbName + 'course_text>' + space + quotes + json[i].course_text_en + quotes + lang + newline; //Text
		createdString += localURI + space + dbName + 'course_url>' + space + '<' + json[i].course_ext_url + '>' + newline; //URL TESTME: if not course url Existent error
		createdString += localURI + space + dbName + 'course_email>' + space + quotes + json[i].course_email + quotes + newline; //Email
		createdString += localURI + space + dbName + 'course_ects>' + space + quotes + json[i].ects + quotes + newline; //Ects
		//TODO Ask about gr and the program name in greece
		createdString += localURI + space + dbName + 'course_program_name>' + space + quotes + json[i].program_name + quotes + '@gr' + newline; //Program Name
		for (let j = 0; j < json[i].required_en.length; j++) {
			createdString += localURI + space + dbName + 'course_required>' + space + URI + json[i].required_en[j] + '>' + newline; //Required
		}
		for (let j = 0; j < json[i].suggested_en.length; j++) {
			createdString += localURI + space + dbName + 'course_suggested>' + space + URI + json[i].suggested_en[j] + '>' + newline; //Suggested
		}

		createdString += localURI + space + dbName + 'course_area_name>' + space + quotes + json[i].area_name_en + quotes + lang + newline; //Area Name
		createdString += localURI + space + dbName + 'course_area_code>' + space + quotes + json[i].area_code_en + quotes + lang + newline; //Area code
	}
	return createdString;
}

function parsePeople(json) {
	var createdString = '';
	for (let i = 0; i < json.length; i++) {
		var localURI = URI + json[i].people_name_en.replace(/\s/g, '') + '>';
		switch (json[i].position_name_en) {
			case 'Associate Professors':
				type = 'Associate_Professors'
				break;
			case 'Assistant Professors':
				type = 'Assistant_Professors';
				break;
			case 'Visiting Professors':
				type = 'Visiting_Professors';
				break;
			default:
				type = json[i].position_name_en;
		}
		createdString += localURI + ' rdf:type ' + URI + type + '>' + newline; //Type
		createdString += localURI + space + dbName + 'people_email>' + space + quotes + json[i].people_email + quotes + newline; //Email
		createdString += localURI + space + dbName + 'people_text>' + space + quotes + json[i].people_text_en + quotes + lang + newline; //Text
		createdString += localURI + space + dbName + 'people_url>' + space + '<' + json[i].people_ext_url + '>' + newline; //External URL TESTME: if not peolpe url Existent error
		createdString += localURI + space + dbName + 'people_name>' + space + quotes + json[i].people_name_en + quotes + lang + newline; //Name
		createdString += localURI + space + dbName + 'people_position_name>' + space + quotes + json[i].position_name_en + quotes + lang + newline; //Position Name
	}
	return createdString;
}

function parseProgrammingLanguage(json) {
	var createdString = '';
	for (let i = 0; i < json.length; i++) {
		var localURI = URI + json[i].course_code_en + '>';
		for (let j = 0; j < json[i].language.length; j++) {
			createdString += localURI + space + dbName + 'programming_language>' + space + URI + json[i].language[j].replace(/\s/g, '_') + '>' + newline;
		}

	}
	return createdString;
}

function parseSubjects(jsou) {
	var createdString = '';
	for (let i = 0; i < json.length; i++) {
		var localURI = URI + json[i].course_code_en + '>';
		for (let j = 0; j < json[i].subjects.length; j++) {
			//TODO: prosthesa edo ta @en
			createdString += localURI + space + dbName + 'subject>' + space + URI + json[i].subjects[j].replace(/\s/g, '_') + '>' + newline;
		}

	}
	return createdString;
}